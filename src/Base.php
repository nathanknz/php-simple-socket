<?php

namespace Nathanknz\SimpleSocket;

use Nathanknz\SimpleSocket\Exception\SocketException;

abstract class Base
{
    protected $socket;

    public function __construct($socket = null)
    {
        if ($socket) {
            $this->socket = $socket;
        } else {
            $this->socket = @socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
        }

        if (!$this->socket) {
            throw new SocketException($this->lastErrorString(), $this->lastError());
        }
    }

    public function getSocket()
    {
        return $this->socket;
    }

    public function clearError()
    {
        return socket_clear_error($this->socket);
    }

    public function lastError()
    {
        return socket_last_error($this->socket);
    }

    public function lastErrorString()
    {
        return socket_strerror(socket_last_error($this->socket));
    }

    public function setBlock()
    {
        @socket_set_block($this->socket);

        return $this;
    }

    public function setNonblock()
    {
        @socket_set_nonblock($this->socket);

        return $this;
    }

    public function setOption($level, $optname, $optval)
    {
        @socket_set_option($this->socket, $optname, $optval);

        return $this;
    }

    public function getOption($level, $optname)
    {
        return @socket_get_option($this->socket, $optname, $optval);
    }

    public function close()
    {
        if ($this->socket) {
            @socket_shutdown($this->socket, 2);
            @socket_close($this->socket);
        }
    }
}
