<?php

namespace Nathanknz\SimpleSocket;

use Nathanknz\SimpleSocket\Exception\SocketException;

class Client extends Base
{
    protected $peer_address = null;
    protected $peer_port = null;

    public function __destruct()
    {
        $this->close();
    }

    public function connect($address, $port = 0)
    {
        $this->clearError();

        $socket_connect = @socket_connect($this->socket, $address, $port);

        if ($socket_connect === false) {
            throw new SocketException($this->lastErrorString(), $this->lastError());
        }

        return $socket_connect;
    }

    public function read($length)
    {
        $this->clearError();

        $socket_read = @socket_read($this->socket, $length);

        if ($socket_read === false) {
            if ($this->lastError() == SOCKET_EWOULDBLOCK) {
                return '';
            } else {
                throw new SocketException($this->lastErrorString(), $this->lastError());
            }
        }

        return $socket_read;
    }

    public function write($buffer, $length = null)
    {
        $length = isset($length) ? $length : strlen($buffer);

        $this->clearError();

        $socket_write = @socket_write($this->socket, $buffer, $length);

        if ($socket_write === false) {
            throw new SocketException($this->lastErrorString(), $this->lastError());
        }

        return $socket_write;
    }

    public function getPeerAddress()
    {
        if (!isset($this->peer_address)) {
            $this->getPeerAddressPort();
        }

        return $this->peer_address;
    }

    public function getPeerPort()
    {
        if (!isset($this->peer_port)) {
            $this->getPeerAddressPort();
        }

        return $this->peer_port;
    }

    protected function getPeerAddressPort()
    {
        $socket_getpeername = @socket_getpeername($this->socket, $this->peer_address, $this->peer_port);

        return $socket_getpeername;
    }
}
