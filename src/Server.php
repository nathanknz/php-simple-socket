<?php

namespace Nathanknz\SimpleSocket;

use Nathanknz\SimpleSocket\Exception\SocketException;

class Server extends Base
{
    public function __construct()
    {
        set_time_limit(0);
        ob_implicit_flush();

        pcntl_signal(SIGCHLD, SIG_IGN);

        parent::__construct();
    }

    public function bind($address, $port)
    {
        $this->clearError();

        $socket_bind = @socket_bind($this->socket, $address, $port);

        if ($socket_bind === false) {
            throw new SocketException($this->lastErrorString(), $this->lastError());
        }

        return $this;
    }

    public function listen($backlog = 16)
    {
        $this->clearError();

        $socket_listen = @socket_listen($this->socket, $backlog);

        if ($socket_listen === false) {
            throw new SocketException($this->lastErrorString(), $this->lastError());
        }

        return $this;
    }

    public function accept($fork = false)
    {
        $this->clearError();

        $client = @socket_accept($this->socket);

        if (!$client) {
            if ($this->lastError()) {
                throw new SocketException($this->lastErrorString(), $this->lastError());
            }
            return false;
        }

        if ($fork && pcntl_fork()) {
            return false;
        }

        return new Client($client);
    }
}
