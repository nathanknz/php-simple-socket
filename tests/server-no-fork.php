<?php

include_once(__DIR__.'/../vendor/autoload.php');

$server = new Nathanknz\SimpleSocket\Server();

$server->bind('0.0.0.0', 12342)->listen();

while (true) {
    echo "[".getmypid()."] Waiting for a client\n";
    $client = $server->accept();
    if ($client) {
        $client->write("[".getmypid()."] Hello World!\n");
        echo "[".getmypid()."] Class: ".get_class($client)."\n";
        sleep(1);
        echo "[".getmypid()."] Closing\n";
        $client->close();
    }
}
